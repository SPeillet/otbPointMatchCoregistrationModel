/*
 * Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplicationFactory.h"

// Elevation handler
#include "otbWrapperElevationParametersHandler.h"

#ifdef OTB_USE_SIFTFAST
#include "otbSiftFastImageFilter.h"
#else
#include "otbImageToSIFTKeyPointSetFilter.h"
#endif
#include "otbImageToSURFKeyPointSetFilter.h"
#include "otbKeyPointSetsMatchingFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbKeyPointSetsMatchingFilter.h"

#include "otbImageToVectorImageCastFilter.h"
#include "otbGenericRSResampleImageFilter.h"
#include "otbBCOInterpolateImageFunction.h"

#include "otbWrapperMapProjectionParametersHandler.h"
#include "otbSensorModelAdapter.h"
#include "otbRPCSolverAdapter.h"
#include "itkEuclideanDistanceMetric.h"

#include "otbGenericRSTransform.h"
#include "otbOGRDataSourceWrapper.h"
#include "ogrsf_frmts.h"

// only need this filter as a dummy process object
#include "otbRGBAPixelConverter.h"

namespace otb
{
namespace Wrapper
{

class PointMatchCoregistrationModel : public Application
{
public:
  /** Standard class typedefs. */
  typedef PointMatchCoregistrationModel   Self;
  typedef Application                     Superclass;
  typedef itk::SmartPointer<Self>         Pointer;
  typedef itk::SmartPointer<const Self>   ConstPointer;

  typedef FloatVectorImageType::PixelType                    RealVectorType;
  typedef FloatVectorImageType::InternalPixelType            ValueType;
  typedef itk::PointSet<RealVectorType,2>                    PointSetType;
  
  // This is ugly ...
  #ifdef OTB_USE_SIFTFAST
  typedef SiftFastImageFilter<FloatImageType,PointSetType>   SiftFilterType;
  #else
  typedef ImageToSIFTKeyPointSetFilter<FloatImageType,PointSetType> SiftFilterType;
  #endif

  typedef otb::RPCSolverAdapter::Point3DType              Point3DType;
  typedef otb::RPCSolverAdapter::Point2DType              Point2DType;
  typedef itk::Statistics::EuclideanDistanceMetric<Point3DType> DistanceType3D;

  typedef otb::RPCSolverAdapter::GCPType           TiePointType;
  typedef otb::RPCSolverAdapter::GCPsContainerType TiePointsType;
  
  typedef itk::Statistics::EuclideanDistanceMetric<RealVectorType> DistanceType;
  typedef otb::KeyPointSetsMatchingFilter<PointSetType,
                                          DistanceType>      MatchingFilterType;
  
  typedef otb::ImageToVectorImageCastFilter<FloatImageType,FloatVectorImageType> CasterType;  
  typedef otb::BCOInterpolateImageFunction<FloatVectorImageType> BCOInterpolatorType;
  typedef otb::GenericRSResampleImageFilter<FloatVectorImageType,FloatVectorImageType>  ResamplerType;
                                          
                                          
  typedef MatchingFilterType::LandmarkListType               LandmarkListType;
  typedef PointSetType::PointType                            PointType;
  typedef PointSetType::PixelType                            PointDataType;
  typedef otb::MultiToMonoChannelExtractROI<ValueType,
                                            ValueType>      ExtractChannelFilterType;
  typedef otb::GenericRSTransform<>                         RSTransformType;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(PointMatchCoregistrationModel, otb::Wrapper::Application);

private:
  void DoInit() override
  {
    SetName("PointMatchCoregistrationModel");
    SetDescription("Compute a RPC model based on tie points between a source and a reference image.");
    SetDocLongDescription("To be done!");
    // Documentation
    SetDocLimitations("Contact Author.");
    SetDocSeeAlso("HomologousPointsExtraction, GenerateRPCModel");
    SetDocAuthors("R. Gaetano (thanks OTB-Team)");

    AddDocTag(Tags::Geometry);

    AddParameter(ParameterType_InputImage, "in", "Input image to coregister");
    SetParameterDescription("in","Input image on which the coregistration model will be computed.");
    AddParameter(ParameterType_Int,"band","Input band on image to coregister for keypoints extraction");
    SetParameterDescription("band","Index of the band from image to coregister for keypoints extraction");
    SetMinimumParameterIntValue("band",1);
    SetDefaultParameterInt("band",1);

    AddParameter(ParameterType_InputImage, "inref", "Reference image");
    SetParameterDescription("inref"," Reference input image");
    AddParameter(ParameterType_Int,"bandref","Input band on target (reference) image for keypoints extraction");
    SetParameterDescription("bandref","Index of the band from target image to use for keypoints extraction");
    SetMinimumParameterIntValue("bandref",1);
    SetDefaultParameterInt("bandref",1);

    AddParameter(ParameterType_Float,"threshold","Distance threshold for matching");
    SetParameterDescription("threshold","The distance threshold for matching.");
    SetMinimumParameterFloatValue("threshold",0.0);
    SetDefaultParameterFloat("threshold",0.6);

    AddParameter(ParameterType_Bool,"backmatching","Use back-matching to filter matches.");
    SetParameterDescription("backmatching","If set to true, matches should be consistent in both ways.");

    AddParameter(ParameterType_Int,"initgeobinsize","Initial size of bin");
    SetParameterDescription("initgeobinsize","Initial radius of the spatial bin in pixels");
    SetDefaultParameterInt("initgeobinsize",256);
    SetMinimumParameterIntValue("initgeobinsize",1);
    
    AddParameter(ParameterType_Int,"initgeobinstep","Initial steps between bins");
    SetParameterDescription("initgeobinstep","Initial size of steps between bins in pixels");
    SetDefaultParameterInt("initgeobinstep",256);
    SetMinimumParameterIntValue("initgeobinstep",1);

    AddParameter(ParameterType_Int,"mingeobinstep","Minimal step between bins");
    SetParameterDescription("mingeobinstep","Minimal size of steps between bins in pixels");
    SetDefaultParameterInt("mingeobinstep",16);
    SetMinimumParameterIntValue("mingeobinstep",1);

    AddParameter(ParameterType_Int,"minsiftpoints","Minimum number of sift point");
    SetParameterDescription("minsiftpoints","Minimum number of sift point");
    SetMinimumParameterIntValue("minsiftpoints",20);
    SetDefaultParameterInt("minsiftpoints",40);
    
    AddParameter(ParameterType_Int,"margin","Margin from image border to start/end bins (in pixels)");
    SetParameterDescription("margin","Margin from image border to start/end bins (in pixels)");
    SetMinimumParameterIntValue("margin",0);
    SetDefaultParameterInt("margin",10);

    AddParameter(ParameterType_Float,"precision","Estimated precision of the colocalisation function (in input image pixels ).");
    SetParameterDescription("precision","Estimated precision of the colocalisation function in pixels");
    SetDefaultParameterFloat("precision",0.);

    AddParameter(ParameterType_Bool,"mfilter","Filter points according to geographical or sensor based colocalisation");
    SetParameterDescription("mfilter","If enabled, this option allows one to filter matches according to colocalisation from sensor or geographical information, using the given tolerance expressed in pixels");
    
    AddParameter(ParameterType_Bool,"resample","Resample input to reference spatial resolution");
    SetParameterDescription("resample","If enabled, input geobins are superimposed to reference ones prior to matching.");

    AddParameter(ParameterType_Bool,"iterate","Resample input to reference spatial resolution");
    SetParameterDescription("iterate","If enabled, iterate on the geobin step to find more homologous points.");
    SetDefaultParameterInt("iterate",1);
    
    // Elevation
    ElevationParametersHandler::AddElevationParameters(this, "elev");

    AddParameter(ParameterType_OutputFilename,"outgeom","Output RPC model");
    SetParameterDescription("outgeom","Geom file containing the point match based RPC model for the source image");
    
    AddParameter(ParameterType_OutputFilename,"outvector","Output vector file with final tie points");
    SetParameterDescription("outvector","File containing segments representing matches between tie points.");
    MandatoryOff("outvector");
    DisableParameter("outvector");

    // Doc example parameter settings
    SetDocExampleParameterValue("in", "source_img.tif");
    SetDocExampleParameterValue("inref","target_img.tif");
    SetDocExampleParameterValue("outgeom","coregistration.geom");

    //SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {

  }

  void Match(FloatImageType * im1, FloatImageType * im2, unsigned int precision, ResamplerType * resampler, RSTransformType * rsTransform, RSTransformType * rsTransformToWGS84, RSTransformType * rsTransformRefToWGS84, TiePointsType & tiepoints, OGRMultiLineString * mls = ITK_NULLPTR)
  {
    MatchingFilterType::Pointer matchingFilter = MatchingFilterType::New();
    SiftFilterType::Pointer sift1 = SiftFilterType::New();
    SiftFilterType::Pointer sift2 = SiftFilterType::New();
    
    if (GetParameterInt("resample")) {
      CasterType::Pointer caster = CasterType::New();
      ExtractChannelFilterType::Pointer extr = ExtractChannelFilterType::New();
      
      if ( (int)precision > GetParameterInt("precision")){
          sift1->SetInput(im1);
          sift1->Update();
          //Specific resampler parameter
          caster->SetInput(im2);
          resampler->SetInput(caster->GetOutput());
            
          resampler->SetOutputOrigin(im1->GetOrigin());
          resampler->SetOutputSpacing(im1->GetSignedSpacing());
          resampler->SetOutputSize(im1->GetLargestPossibleRegion().GetSize());
          resampler->SetOutputStartIndex(im1->GetLargestPossibleRegion().GetIndex());
          
          extr->SetInput(resampler->GetOutput());
          extr->SetChannel(1);

          sift2->SetInput(extr->GetOutput());
          sift2->Update();
        }
      else if ( (int)precision < GetParameterInt("precision")){
          sift2->SetInput(im2);
          sift2->Update();
          //Specific resampler parameter
          caster->SetInput(im1);
          resampler->SetInput(caster->GetOutput());
            
          resampler->SetOutputOrigin(im2->GetOrigin());
          resampler->SetOutputSpacing(im2->GetSignedSpacing());
          resampler->SetOutputSize(im2->GetLargestPossibleRegion().GetSize());
          resampler->SetOutputStartIndex(im2->GetLargestPossibleRegion().GetIndex());
          
          extr->SetInput(resampler->GetOutput());
          extr->SetChannel(1);

          sift1->SetInput(extr->GetOutput());
          sift1->Update();
        }
      else {
          sift1->SetInput(im1);
          sift1->Update();
          sift2->SetInput(im2);
          sift2->Update();
        }
    }
    else {
      sift1->SetInput(im1);
      sift1->Update();
      sift2->SetInput(im2);
      sift2->Update();
    }
    
    matchingFilter->SetInput1(sift1->GetOutput());
    matchingFilter->SetInput2(sift2->GetOutput());
    matchingFilter->SetDistanceThreshold(GetParameterFloat("threshold"));
    matchingFilter->SetUseBackMatching(GetParameterInt("backmatching"));

    try
      {

      matchingFilter->Update();

      LandmarkListType::Pointer landmarks = matchingFilter->GetOutput();

      unsigned int discarded  = 0;

      for (LandmarkListType::Iterator it = landmarks->Begin();
           it != landmarks->End(); ++it)
        {
        Point2DType p1rpc;
        Point3DType p2rpc;
        PointType point1 = it.Get()->GetPoint1();
        PointType point2 = it.Get()->GetPoint2();
        FloatImageType::VectorType vec = im1->GetGeoTransform();
        p1rpc[0] = (point1[0]-vec[0])/vec[1];
        p1rpc[1] = (point1[1]-vec[3])/vec[5];
        
        double error = 0;
        PointType pprime1,pprime2;

        bool filtered = false;

        if(GetParameterInt("mfilter"))
          {
          pprime1 = rsTransform->TransformPoint(point1);
          error = vcl_sqrt((point2[0]-pprime1[0])*(point2[0]-pprime1[0])+(point2[1]-pprime1[1])*(point2[1]-pprime1[1]));

          if(error>static_cast<float>(precision)*vcl_sqrt(vcl_abs(im2->GetSignedSpacing()[0]*im2->GetSignedSpacing()[1])))
            {
            filtered = true;
            }
          }

        if(!filtered)
          {
            pprime2 = rsTransformToWGS84->TransformPoint(point2);
            p2rpc[0] = pprime2[0];
            p2rpc[1] = pprime2[1];
            p2rpc[2] = otb::DEMHandler::Instance()->GetHeightAboveEllipsoid(pprime2[0],pprime2[1]);
            tiepoints.push_back(std::make_pair(p1rpc,p2rpc));
            
            if(mls)
            {
             pprime1 = rsTransformRefToWGS84->TransformPoint(point1);

             OGRLineString ls;
             ls.addPoint(pprime1[0],pprime1[1]);
             ls.addPoint(pprime2[0],pprime2[1]);
             mls->addGeometry(&ls);
            }
            
          }
        else
          {
          ++discarded;
          }
        }
      }

    catch(itk::ExceptionObject &)
      {
      // silent catch
      }
  }


  void DoExecute() override
  {
    OGRMultiLineString mls;

    FloatVectorImageType::Pointer image1 = this->GetParameterImage("in");
    FloatVectorImageType::Pointer image2 = this->GetParameterImage("inref");

    // Setting up RS Transform
    RSTransformType::Pointer rsTransform = RSTransformType::New();
    rsTransform->SetInputKeywordList(image1->GetImageKeywordlist());
    rsTransform->SetInputProjectionRef(image1->GetProjectionRef());
    rsTransform->SetOutputKeywordList(image2->GetImageKeywordlist());
    rsTransform->SetOutputProjectionRef(image2->GetProjectionRef());

    RSTransformType::Pointer rsTransformToWGS84 = RSTransformType::New();
    rsTransformToWGS84->SetInputKeywordList(image2->GetImageKeywordlist());
    rsTransformToWGS84->SetInputProjectionRef(image2->GetProjectionRef());
    
    RSTransformType::Pointer rsTransformRefToWGS84 = RSTransformType::New();
    rsTransformRefToWGS84->SetInputKeywordList(image1->GetImageKeywordlist());
    rsTransformRefToWGS84->SetInputProjectionRef(image1->GetProjectionRef());

    // Setting up channel extractors
    ExtractChannelFilterType::Pointer extractChannel1 = ExtractChannelFilterType::New();
    extractChannel1->SetInput(image1);
    extractChannel1->SetChannel(GetParameterInt("band"));

    ExtractChannelFilterType::Pointer extractChannel2 = ExtractChannelFilterType::New();
    extractChannel2->SetInput(image2);
    extractChannel2->SetChannel(GetParameterInt("bandref"));
    
    ResamplerType::Pointer m_Resampler = ResamplerType::New();
    BCOInterpolatorType::Pointer interpolator = BCOInterpolatorType::New();
    interpolator->SetRadius(2);
    m_Resampler->SetInterpolator(interpolator);

    // Setup the DEM Handler
    otb::Wrapper::ElevationParametersHandler::SetupDEMHandlerFromElevationParameters(this,"elev");

    rsTransform->InstantiateTransform();
    rsTransformToWGS84->InstantiateTransform();
    rsTransformRefToWGS84->InstantiateTransform();
    
    TiePointsType tiepoints;
    
    float img_px_area = vcl_sqrt(vcl_abs(image1->GetSignedSpacing()[0] * image1->GetSignedSpacing()[1]));
    float ref_px_area = vcl_sqrt(vcl_abs(image2->GetSignedSpacing()[0] * image2->GetSignedSpacing()[1]));

    //Global resampler parameters
    FloatImageType::SpacingType defSpacing;
    if (img_px_area > ref_px_area) {
        defSpacing[0] = 10 * image1->GetSignedSpacing()[0];
        defSpacing[1] = 10 * image1->GetSignedSpacing()[1];
        m_Resampler->SetDisplacementFieldSpacing(defSpacing);
        m_Resampler->SetInputKeywordList(image2->GetImageKeywordlist());
        m_Resampler->SetInputProjectionRef(image2->GetProjectionRef());
        m_Resampler->SetOutputKeywordList(image1->GetImageKeywordlist());
        m_Resampler->SetOutputProjectionRef(image1->GetProjectionRef());
    } else {
        defSpacing[0] = 10 * image2->GetSignedSpacing()[0];
        defSpacing[1] = 10 * image2->GetSignedSpacing()[1];
        m_Resampler->SetDisplacementFieldSpacing(defSpacing);
        m_Resampler->SetInputKeywordList(image1->GetImageKeywordlist());
        m_Resampler->SetInputProjectionRef(image1->GetProjectionRef());
        m_Resampler->SetOutputKeywordList(image2->GetImageKeywordlist());
        m_Resampler->SetOutputProjectionRef(image2->GetProjectionRef());
    }
    // ---
    
    // Compute binning on first image
    FloatImageType::SizeType size = image1->GetLargestPossibleRegion().GetSize();
    unsigned int bin_size_x = GetParameterInt("initgeobinsize");
    unsigned int bin_size_y = bin_size_x;

    unsigned int image_border_margin = GetParameterInt("margin");
    
    unsigned int step = GetParameterInt("initgeobinstep");
    unsigned int minstep;
    if (GetParameterInt("iterate")) {
        minstep = GetParameterInt("mingeobinstep");
    }
    else {
        minstep = step;
    }
    unsigned int npts = 0;
    
    unsigned int minsiftpoints = static_cast<unsigned int>(GetParameterInt("minsiftpoints"));
    
    float prec = GetParameterFloat("precision");
    float res_ratio = img_px_area / ref_px_area;
    prec *= res_ratio;
    unsigned int iprec = static_cast<unsigned int>(vcl_ceil(prec));
    otbAppLogINFO("Using an actual precision of " << iprec << " pixels on reference image.");
    otbAppLogINFO("Initial geobins step of " << step );
    
    while (step >= minstep && npts < minsiftpoints)
      {
        unsigned int bin_step_x = step;
        unsigned int bin_step_y = bin_step_x;

        unsigned int nb_bins_x = static_cast<unsigned int>(vcl_ceil(static_cast<float>(size[0]-2*image_border_margin)/(bin_size_x + bin_step_x)));
        unsigned int nb_bins_y = static_cast<unsigned int>(vcl_ceil(static_cast<float>(size[1]-2*image_border_margin)/(bin_size_y + bin_step_y)));
          
        RGBAPixelConverter<int,int>::Pointer dummyFilter = RGBAPixelConverter<int,int>::New();
        dummyFilter->SetProgress(0.0f);
        this->AddProcess(dummyFilter,"Homologous points extraction...");
        dummyFilter->InvokeEvent(itk::StartEvent());
          
        float tot_bins = (float)(nb_bins_x * nb_bins_y);
        float count_bins = 0.0f;

        for(unsigned int i = 0; i<nb_bins_x; ++i)
          {
          for(unsigned int j = 0; j<nb_bins_y; ++j)
            {
            unsigned int startx = image_border_margin + i*(bin_size_x + bin_step_x);
            unsigned int starty = image_border_margin + j*(bin_size_y + bin_step_y);
            
            FloatImageType::SizeType size1;
            FloatImageType::IndexType index1;
            FloatImageType::RegionType region1;

            index1[0]=startx;
            index1[1]=starty;
            size1[0] = bin_size_x;
            size1[1] = bin_size_y;

            region1.SetIndex(index1);
            region1.SetSize(size1);

            FloatImageType::RegionType largestRegion = image1->GetLargestPossibleRegion();
            
            largestRegion.ShrinkByRadius(image_border_margin);
            region1.Crop(largestRegion);

            extractChannel1->SetExtractionRegion(region1);

            // We need to find the corresponding region in reference image
            itk::ContinuousIndex<double,2> ul_i, ur_i, lr_i, ll_i;
            FloatImageType::PointType ul1, ur1, ll1, lr1, p1, p2, p3, p4;
            itk::ContinuousIndex<double,2> i1, i2, i3, i4, i_min, i_max;

            ul_i[0] = static_cast<double>(startx) - 0.5;
            ul_i[1] = static_cast<double>(starty) - 0.5;
            ur_i = ul_i;
            lr_i = ul_i;
            ll_i = ul_i;
            ur_i[0] += static_cast<double>(bin_size_x);
            lr_i[0] += static_cast<double>(bin_size_x);
            lr_i[1] += static_cast<double>(bin_size_y);
            ll_i[1] += static_cast<double>(bin_size_y);

            image1->TransformContinuousIndexToPhysicalPoint(ul_i,ul1);
            image1->TransformContinuousIndexToPhysicalPoint(ur_i,ur1);
            image1->TransformContinuousIndexToPhysicalPoint(lr_i,lr1);
            image1->TransformContinuousIndexToPhysicalPoint(ll_i,ll1);

            p1 = rsTransform->TransformPoint(ul1);
            p2 = rsTransform->TransformPoint(ur1);
            p3 = rsTransform->TransformPoint(lr1);
            p4 = rsTransform->TransformPoint(ll1);

            image2->TransformPhysicalPointToContinuousIndex(p1,i1);
            image2->TransformPhysicalPointToContinuousIndex(p2,i2);
            image2->TransformPhysicalPointToContinuousIndex(p3,i3);
            image2->TransformPhysicalPointToContinuousIndex(p4,i4);

            i_min[0] = std::min(std::min(i1[0],i2[0]),std::min(i3[0],i4[0]));
            i_min[1] = std::min(std::min(i1[1],i2[1]),std::min(i3[1],i4[1]));

            i_max[0] = std::max(std::max(i1[0],i2[0]),std::max(i3[0],i4[0]));
            i_max[1] = std::max(std::max(i1[1],i2[1]),std::max(i3[1],i4[1]));

            FloatImageType::IndexType index2;
            FloatImageType::SizeType size2;

            index2[0] = vcl_floor(i_min[0]);
            index2[1] = vcl_floor(i_min[1]);

            size2[0] = vcl_ceil(i_max[0]-i_min[0]);
            size2[1] = vcl_ceil(i_max[1]-i_min[1]);

            FloatImageType::RegionType region2;
            region2.SetIndex(index2);
            region2.SetSize(size2);
            region2.PadByRadius(iprec);

            FloatImageType::RegionType largestRegion2 = image2->GetLargestPossibleRegion();
            largestRegion2.ShrinkByRadius(image_border_margin);

            if(region2.Crop(largestRegion2))
              {
              extractChannel2->SetExtractionRegion(region2);
              Match(extractChannel1->GetOutput(),extractChannel2->GetOutput(),iprec,m_Resampler,rsTransform,rsTransformToWGS84,rsTransformRefToWGS84,tiepoints,&mls);
              }

            count_bins += 1.0f;
            dummyFilter->UpdateProgress(count_bins/tot_bins);
            }
          }
          dummyFilter->InvokeEvent(itk::EndEvent());
          npts = tiepoints.size();
          if (npts <= minsiftpoints)
            {
                step /= 2;
                if (step >= minstep){
                otbAppLogINFO("Found only " << npts << " tie points. Retrying with step " << step);
                }
            }
      }
      otbAppLogINFO("Found " << tiepoints.size() << " tie points. Computing RPC model...");

      double rms;
      otb::RPCSolverAdapter::Solve(tiepoints,rms,GetParameterString("outgeom"));
      otbAppLogINFO("Done.\n");
      
      if(IsParameterEnabled("outvector"))
        {
        // Create the datasource (for matches export)
        otb::ogr::Layer layer(ITK_NULLPTR, false);
        otb::ogr::DataSource::Pointer ogrDS;

        ogrDS = otb::ogr::DataSource::New(GetParameterString("outvector"), otb::ogr::DataSource::Modes::Overwrite);
        std::string projref = "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]";
        OGRSpatialReference oSRS(projref.c_str());

        // and create the layer
        layer = ogrDS->CreateLayer("matches", &oSRS, wkbMultiLineString);
        OGRFeatureDefn & defn = layer.GetLayerDefn();
        ogr::Feature feature(defn);

        feature.SetGeometry(&mls);
        layer.CreateFeature(feature);
        }
  }
  
};
}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::PointMatchCoregistrationModel)
